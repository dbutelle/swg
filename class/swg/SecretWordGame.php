<?php
namespace swg ;

class SecretWordGame
{

    private const HIDDEN_CHAR = '?' ;

    private array $secret ;

    public function __construct(string $secret)
    {
        $this->secret = str_split(strtolower($secret), 1);
    }

    public function try(?array $word=null): array{

        $result = "" ;
        $index = 0 ;

        if (isset($word)){
            for ($i=0 ; $i<count($word) ; $i++) $word[$i] = strtolower($word[$i]) ;
            for ($index; $index < count($this->secret); $index++) {
                if($this->secret[$index] == " "){
                    $result .= " " ;
                } elseif (isset($word[$index])) {
                    if ($this->secret[$index] == $word[$index]) {
                        $result .= $this->secret[$index];
                    } else {
                        $result .= self::HIDDEN_CHAR;
                    }
                } else {
                    break;
                }
            }
        }

        if ($index < count($this->secret)) {
            for ($index; $index < count($this->secret); $index++) {
                if($this->secret[$index] == " ") $result .= " " ;
                else $result .= self::HIDDEN_CHAR;
            }
        }

        $win = join("", $word) == join("", $this->secret) ;
        return $this->generateResponse($word, $win, $result) ;

    }

    private function generateResponse(array $word, bool $win, string $result){
        return array(
            'word' => $word,
            'win' => $win,
            'result' => $result
        );
    }

    public function generateInput(?array $response): void{


        $lastWord = $response['word'] ;
        $resultat = $response['result'];
        for ($i = 0; $i  < count($this->secret); $i ++) {
            $name = "letter[".$i."]";
            if($resultat[$i ] == "?")
            {
                $letter = (empty($lastWord[$i]))? "" : $lastWord[$i];
                echo"
                <input type='text' class='secret-letter-input' name='$name' value='$letter'>
                ";
            }
            else
            {
                $letter = $resultat[$i];
                if($letter == " ")
                {
                    echo "
                    <input type='hidden' name='$name'' value='$letter'>
                    <div class='secret-letter-separator'></div>
                    ";
                }
                else
                {
                    echo "<input type='text' class='secret-letter-input' name='$name'' value='$letter' disabled>
                    <input type='hidden' name='$name' value='$letter'>"
                    ;
                }
            }
        }
    }

    public function generateWin() : void{
        echo "<div id='secret-word'>".join("", $this->secret)."</div>" ;
        echo "<div id='secret-word-win'>!!! YOU WIN !!!</div>" ;
    }

}